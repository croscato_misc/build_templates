===============
Build templates
===============

A start project for various build systems.

About
=====

Provides a simple project template, for various build systems, that can be customized to the users needs.

Author
======

* **Gustavo Ribeiro Croscato** - `croscato`_

License
=======

AVR Proj is licensed under the `MIT license`_.

.. _croscato: https://gitlab.com/croscato
.. _MIT license: https://mit-license.org
