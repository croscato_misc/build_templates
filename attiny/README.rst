========
AVR Proj
========

A base projeto for ATtiny microcontrolers

About
=====


Author
======

* **Gustavo Ribeiro Croscato** - `croscato`_

License
=======

AVR Proj is licensed under the `MIT license`_.

.. _croscato: https://gitlab.com/croscato
.. _MIT license: https://mit-license.org
