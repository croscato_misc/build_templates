# Copyright (c) 2023 Gustavo Ribeiro Croscato
# SPDX-License-Identifier: MIT

include(mcu/serie/avr25)

get_filename_component(MCU_MODEL ${CMAKE_CURRENT_LIST_FILE} NAME_WLE)
string(TOUPPER ${MCU_MODEL} MCU_MODEL)

set(MCU_MODEL ${MCU_MODEL} CACHE INTERNAL "Microcontroller model")
set(MCU_GENERIC ATTINY2313 CACHE INTERNAL "Microcontroller generic model")
set(MCU_AVRDUDE_PART t2313 CACHE INTERNAL "Microcontroller part in avrdude")
set(MCU_FLASH_SIZE 2K CACHE INTERNAL "${MCU_MODEL} Flash size")
set(MCU_SRAM_SIZE 128 CACHE INTERNAL "${MCU_MODEL} SRAM size")
set(MCU_EEPROM_SIZE 128 CACHE INTERNAL "${MCU_MODEL} EEPROM size")
set(MCU_CLOCK 20MHz CACHE INTERNAL "${MCU_MODEL} Max CPU speed")
