# Copyright (c) 2023 Gustavo Ribeiro Croscato
# SPDX-License-Identifier: MIT

get_filename_component(MCU_SERIE ${CMAKE_CURRENT_LIST_FILE} NAME_WLE)
string(TOUPPER ${MCU_SERIE} MCU_SERIE)

set(MCU_SERIE ${MCU_SERIE} CACHE INTERNAL "Microcontroller serie")
