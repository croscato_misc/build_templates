// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#ifndef AVRPROJ_SRC_DEFS_HPP
#define AVRPROJ_SRC_DEFS_HPP 1

#pragma once

#include <stdint.h>
#include <stddef.h>

#define UNUSED(param) ((void)(param))

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

using s8 = int8_t;
using s16 = int16_t;
using s32 = int32_t;
using s64 = int64_t;

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using f32 = float;

using iptr = intptr_t;
using sptr = intptr_t;
using uptr = uintptr_t;

using isize = intptr_t;
using ssize = intptr_t;
using usize = size_t;

static_assert(sizeof(f32) == 4);
static_assert(sizeof(iptr) == sizeof(u16));
static_assert(sizeof(uptr) == sizeof(u16));
static_assert(sizeof(ssize) == sizeof(usize));

#endif // AVRPROJ_SRC_DEFS_HPP 1

