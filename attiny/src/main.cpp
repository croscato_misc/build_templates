// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#include "defs.hpp"

#include <avr/io.h>
#include <util/delay.h>

/*
                         ATtiny 2313 - PDIP/SOIC
                                 +---u---+
                  (RESET/dW) PA2 |1    20| VCC
                       (RXD) PD0 |2    19| PB7 (UCSK/SCL/PCINT7)
                       (TXD) PD1 |3    18| PB6 (MISO/DO/PCINT6)
                     (XTAL2) PA1 |4    17| PB5 (MOSI/DI/SDA/PCINT5)
                     (XTAL1) PA0 |5    16| PB4 (OC1B/PCINT4)
            (CKOUT/XCK/INT0) PD2 |6    15| PB3 (OC1A/PCINT3)
                      (INT1) PD3 |7    14| PB2 (OC0A/PCINT2)
                        (T0) PD4 |8    13| PB1 (AIN1/PCINT1)
                   (OC0B/T1) PD5 |8    12| PB0 (AIN2/PCINT0)
                             GND |10   11| PD6 (ICP)
                                 +-------+
*/

using u8 = uint8_t;

constexpr u8 LED_PIN { PD1 };

int
main(void)
{
    constexpr u8 LED_MASK = (1u << LED_PIN);

    DDRD = LED_MASK;
    PORTD = 0b00000000;

    for (;;) {
        PORTD ^= LED_MASK;
        _delay_ms(500);
    }
}
