// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#include <iostream>

int
main(void)
{
    std::cout << "Hello, world!\n";

    return 0;
}


