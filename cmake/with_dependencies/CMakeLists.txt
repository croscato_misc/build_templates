# Copyright (c) 2023 Gustavo Ribeiro Croscato
# SPDX-License-Identifier: MIT

cmake_minimum_required(VERSION 3.18 FATAL_ERROR)

project(cmake_template LANGUAGES C CXX)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(USE_GFX none CACHE STRING "Building with gfx backend")
set_property(CACHE USE_GFX PROPERTY STRINGS none sdl2 glfw)

option(USE_FMT "Build fmt library" ON)
option(USE_SPDLOG "Build spdlog library" OFF)
option(USE_GLM "Enable glm library" OFF)
option(USE_GLAD2 "Build glad2 library" OFF)
option(USE_SQLITE3 "Build sqlite3 library" OFF)
option(USE_STB_IMAGE "Build stb_image library" OFF)
option(USE_IMGUI "Build imgui library" OFF)

if(${USE_IMGUI} AND NOT ${USE_GFX} STREQUAL sdl2 AND NOT ${USE_GFX} STREQUAL glfw)
    message(FATAL_ERROR "When using imgui you must choose a GFX backend (sdl2 or glfw)")
endif()

if(${USE_GFX} STREQUAL sdl2 OR ${USE_GFX} STREQUAL glfw)
    set(USE_GLAD2 ON)
    set(USE_SPDLOG ON)

    if(${USE_GFX} STREQUAL sdl2)
        set(USE_SDL2 ON)
        set(USE_GLFW OFF)
    elseif(${USE_GFX} STREQUAL glfw)
        set(USE_SDL2 OFF)
        set(USE_GLFW ON)
    endif()
endif()

add_subdirectory(src)

set(FETCHCONTENT_FULLY_DISCONNECTED ON CACHE BOOL "Disable dependency update." FORCE)
