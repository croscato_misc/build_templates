# Copyright (c) 2023 Gustavo Ribeiro Croscato
# SPDX-License-Identifier: MIT

include(FindGit)

find_package(Git)

if (NOT Git_FOUND)
    message(FATAL_ERROR "Git not found!")
endif ()

include(FetchContent)

set(FETCHCONTENT_QUIET FALSE)

FetchContent_Declare(sdl2
    GIT_REPOSITORY https://github.com/libsdl-org/SDL.git
    GIT_TAG release-2.26.5
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_SDL2)
    set(SDL_SHARED ON CACHE BOOL "Build SDL2 shared library" FORCE)
    set(SDL_STATIC OFF CACHE BOOL "Build SDL2 static library" FORCE)
    set(SDL2_DISABLE_SDL2MAIN ON CACHE BOOL "Enable SDL2Main" FORCE)

    FetchContent_MakeAvailable(sdl2)

    add_compile_definitions(USING_SDL2)
endif()

FetchContent_Declare(glfw
    GIT_REPOSITORY https://github.com/glfw/glfw.git
    GIT_TAG 3.3.8
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_GLFW)
    set(BUILD_SHARED_LIBS ON)
    set(GLFW_BUILD_EXAMPLES OFF)
    set(GLFW_BUILD_TESTS OFF)
    set(GLFW_BUILD_DOCS OFF)
    set(GLFW_INSTALL OFF)

    FetchContent_MakeAvailable(glfw)

    add_compile_definitions(USING_GLFW)
endif()

FetchContent_Declare(glad2
    GIT_REPOSITORY https://github.com/Dav1dde/glad.git
    GIT_TAG glad2
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_GLAD2)
    FetchContent_MakeAvailable(glad2)

    include(${glad2_SOURCE_DIR}/cmake/CMakeLists.txt)

    glad_add_library(glad2 STATIC QUIET API gl:core=3.3)

    add_compile_definitions(USING_GLAD2)
endif()

FetchContent_Declare(fmt
    GIT_REPOSITORY https://github.com/fmtlib/fmt.git
    GIT_TAG 9.1.0
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_FMT)
    set(FMT_DOC OFF)
    set(FMT_INSTALL OFF)
    set(FMT_TEST OFF)

    FetchContent_MakeAvailable(fmt)

    add_compile_definitions(USING_FMT)
endif()

FetchContent_Declare(spdlog
    GIT_REPOSITORY https://github.com/gabime/spdlog.git
    GIT_TAG v1.11.0
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_SPDLOG)
    set(SPDLOG_INSTALL OFF)
    set(SPDLOG_FMT_EXTERNAL ${USE_FMT})

    FetchContent_MakeAvailable(spdlog)

    add_compile_definitions(USING_SPDLOG)
endif()

FetchContent_Declare(glm
    GIT_REPOSITORY https://github.com/g-truc/glm.git
    GIT_TAG 0.9.9.8
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_GLM)
    FetchContent_MakeAvailable(glm)

    add_compile_definitions(USING_GLM)
endif()

FetchContent_Declare(sqlite3
    GIT_REPOSITORY https://github.com/sqlite/sqlite.git
    GIT_TAG version-3.41.2
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_SQLITE3)
    FetchContent_Populate(sqlite3)

    if(NOT sqlite3_CONFIGURED)
        set(sqlite3_CONFIGURED TRUE CACHE BOOL "SQlite3 is configured" FORCE)

        execute_process(COMMAND ./configure WORKING_DIRECTORY ${sqlite3_SOURCE_DIR})
        execute_process(COMMAND make sqlite3.c WORKING_DIRECTORY ${sqlite3_SOURCE_DIR})

        configure_file(
            ${CMAKE_SOURCE_DIR}/generated/sqlite3_cmakelists.txt.in
            ${sqlite3_SOURCE_DIR}/CMakeLists.txt
            @ONLY
        )
    endif()

    add_subdirectory(${sqlite3_SOURCE_DIR} ${sqlite3_BINARY_DIR})
endif()

FetchContent_Declare(stb_image
    GIT_REPOSITORY https://github.com/nothings/stb.git
    GIT_TAG master
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_STB_IMAGE)
    FetchContent_Populate(stb_image)

    if(NOT stb_image_CONFIGURED)
        set(stb_image_CONFIGURED TRUE CACHE BOOL "stb_image is configured" FORCE)

        configure_file(
            ${CMAKE_SOURCE_DIR}/generated/stb_image_c.in
            ${stb_image_SOURCE_DIR}/stb_image.c
            @ONLY
        )

        configure_file(
            ${CMAKE_SOURCE_DIR}/generated/stb_image_cmakelists.txt.in
            ${stb_image_SOURCE_DIR}/CMakeLists.txt
            @ONLY
        )
    endif()

    add_subdirectory(${stb_image_SOURCE_DIR} ${stb_image_BINARY_DIR})
endif()

FetchContent_Declare(imgui
    GIT_REPOSITORY https://github.com/ocornut/imgui.git
    GIT_TAG v1.89.5
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

if(USE_IMGUI)
    FetchContent_Populate(imgui)

    if(NOT imgui_CONFIGURED)
        set(imgui_CONFIGURED TRUE CACHE BOOL "imgui is configured" FORCE)

        configure_file(
            ${CMAKE_SOURCE_DIR}/generated/imgui_cmakelists.txt.in
            ${imgui_SOURCE_DIR}/CMakeLists.txt
            @ONLY
        )
    endif()

    add_subdirectory(${imgui_SOURCE_DIR} ${imgui_BINARY_DIR})
endif()
