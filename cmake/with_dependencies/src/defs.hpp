// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#ifndef CMAKE_TEMPLATE_SRC_DEFS_HPP
#define CMAKE_TEMPLATE_SRC_DEFS_HPP 1

#pragma once

#include <cinttypes>
#include <cstddef>
#include <cstdlib>
#include <cstdio>

#include <string_view>

#if defined(USING_GLAD2)
#   include <glad/gl.h>
#endif

#if defined(USING_SDL2)
#   include <SDL2/SDL.h>
#endif

#if defined(USING_GLFW)
#   include <GLFW/glfw3.h>
#endif

#if defined(USING_FMT)
#   include <fmt/core.h>
#   include <fmt/format.h>
#endif

#if defined(USING_SPDLOG)
#   include <spdlog/spdlog.h>
#endif

#if defined(USING_GLM)
#   include <glm/vec2.hpp>
#   include <glm/vec3.hpp>
#   include <glm/vec4.hpp>
#   include <glm/mat4x4.hpp>
#   include <glm/ext/matrix_transform.hpp>
#   include <glm/ext/matrix_clip_space.hpp>
#   include <glm/ext/scalar_constants.hpp>
#endif

using i8 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using s8 = std::int8_t;
using s16 = std::int16_t;
using s32 = std::int32_t;
using s64 = std::int64_t;

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using isize = std::int64_t;
using usize = std::uint64_t;

using f32 = float;
using f64 = double;

static_assert(sizeof(f32) == 4);
static_assert(sizeof(f64) == 8);
static_assert(sizeof(isize) == 8);
static_assert(sizeof(usize) == 8);

#define UNUSED(param) ((void)(param))

#endif // CMAKE_TEMPLATE_DEFS_HPP

