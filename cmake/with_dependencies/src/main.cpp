// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

int
main(void)
{
    fmt::print("Hello, world!\n");

    return 0;
}

