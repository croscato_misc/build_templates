// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#if defined(USE_IMGUI)
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#endif

static constexpr i32 kScreenWidth = 1280;
static constexpr i32 kScreenHeight = 720;

enum class LogAction {
    None,
    Abort
};

void
Log_GLFWError(LogAction action = LogAction::None)
{
    const char *description = nullptr;
    int code = glfwGetError(&description);

    spdlog::error("GLFW error: {} - {}.", code, description);

    if (action == LogAction::Abort) {
        abort();
    }
}

void
Log_Error(std::string_view error, LogAction action = LogAction::None)
{
    spdlog::error("Error: {}.", error);

    if (action == LogAction::Abort) {
        abort();
    }
}

int
main(void)
{
    if (!glfwInit()) {
        Log_GLFWError(LogAction::Abort);
    }

    spdlog::info("GLFW application");

    spdlog::info("GLFW compiled version: {}", glfwGetVersionString());

    int major;
    int minor;
    int patch;

    glfwGetVersion(&major, &minor, &patch);

    spdlog::info("GLFW linked version: {}.{}.{}", major, minor, patch);

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow *window = glfwCreateWindow(
        kScreenWidth, kScreenHeight,
        "CMake & GLFW",
        nullptr, nullptr
    );

    if (window == nullptr) {
        Log_GLFWError(LogAction::Abort);
    }

    glfwMakeContextCurrent(window);

    int gl_version = gladLoadGL(glfwGetProcAddress);

    if (gl_version == 0) {
        Log_Error("Can't initialize OpenGL context.", LogAction::Abort);
    }

    spdlog::info("OpenGL vendor: {}", reinterpret_cast<const char *>(glGetString(GL_VENDOR)));
    spdlog::info("OpenGL renderer: {}", reinterpret_cast<const char *>(glGetString(GL_RENDERER)));
    spdlog::info("OpenGL version: {}", reinterpret_cast<const char *>(glGetString(GL_VERSION)));
    spdlog::info("OpenGL GLSL: {}", reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION)));

    glViewport(0, 0, kScreenWidth, kScreenHeight);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    glfwSetKeyCallback(window, [](GLFWwindow *wnd, int key, int scancode, int action, int mods)->void {
        UNUSED(scancode);
        UNUSED(mods);

        if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
            glfwSetWindowShouldClose(wnd, true);
        }
    });

#if defined(USE_IMGUI)
    IMGUI_CHECKVERSION();

    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGuiIO &io = ImGui::GetIO();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 150");

    bool show_demo_window = true;
    bool show_another_window = false;

    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
#endif

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

#if defined(USE_IMGUI)
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window) {
            ImGui::ShowDemoWindow(&show_demo_window);
        }

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {
            static float f = 0.0f;
            static int counter = 0;

            ImGui::Begin("Hello, world!");  // Create a window called "Hello, world!" and append into it.

            ImGui::Text("This is some useful text.");                 // Display some text (you can use a format strings too)
            ImGui::Checkbox("Demo Window", &show_demo_window);        // Edit bools storing our window open/close state
            ImGui::Checkbox("Another Window", &show_another_window);

            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);                                // Edit 1 float using a slider from 0.0f to 1.0f
            ImGui::ColorEdit3("clear color", reinterpret_cast<float *>(&clear_color));  // Edit 3 floats representing a color

            if (ImGui::Button("Button")) {  // Buttons return true when clicked (most widgets return true when edited/activated)
                counter++;
            }

            ImGui::SameLine();
            ImGui::Text("counter = %d", counter);

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", static_cast<f64>(1000.0f / io.Framerate), static_cast<f64>(io.Framerate));
            ImGui::End();
        }

        // 3. Show another simple window.
        if (show_another_window)
        {
            ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
            ImGui::Text("Hello from another window!");

            if (ImGui::Button("Close Me")) {
                show_another_window = false;
            }

            ImGui::End();
        }

        ImGui::Render();
#endif

#if defined(USE_IMGUI)
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
#else
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
#endif

        glClear(GL_COLOR_BUFFER_BIT);

#if defined(USE_IMGUI)
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
#endif

        glfwSwapBuffers(window);
    }

#if defined(USE_IMGUI)
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
#endif

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

