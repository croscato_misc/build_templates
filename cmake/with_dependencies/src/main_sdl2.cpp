// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#if defined(USE_IMGUI)
#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_opengl3.h"
#endif

static constexpr i32 kScreenWidth = 1280;
static constexpr i32 kScreenHeight = 720;

enum class LogAction {
    None,
    Abort
};

void
Log_SDLError(LogAction action = LogAction::None)
{
    spdlog::error("SDL error: {}.", SDL_GetError());

    if (action == LogAction::Abort) {
        abort();
    }
}

void
Log_Error(std::string_view error, LogAction action = LogAction::None)
{
    spdlog::error("Error: {}.", error);

    if (action == LogAction::Abort) {
        abort();
    }
}

int
main(void)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        Log_SDLError(LogAction::Abort);
    }

    spdlog::info("SDL2 application");

    SDL_version sdl_compiled;
    SDL_VERSION(&sdl_compiled);

    spdlog::info("SDL2 compiled version: {}.{}.{}", sdl_compiled.major, sdl_compiled.minor, sdl_compiled.patch);

    SDL_version sdl_linked;
    SDL_GetVersion(&sdl_linked);

    spdlog::info("SDL2 linked version: {}.{}.{}", sdl_linked.major, sdl_linked.minor, sdl_linked.patch);

    SDL_GL_LoadLibrary(nullptr);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, SDL_TRUE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, SDL_TRUE);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    SDL_Window *window = SDL_CreateWindow(
        "CMake with SDL2",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        kScreenWidth, kScreenHeight,
        SDL_WINDOW_OPENGL
    );

    if (window == nullptr) {
        Log_SDLError(LogAction::Abort);
    }

    SDL_GLContext gl_context = SDL_GL_CreateContext(window);

    if (gl_context == nullptr) {
        Log_SDLError(LogAction::Abort);
    }

    SDL_GL_MakeCurrent(window, gl_context);

    int gl_version = gladLoadGL(reinterpret_cast<GLADloadfunc>(SDL_GL_GetProcAddress));

    if (gl_version == 0) {
        Log_Error("Can't initialize OpenGL context.", LogAction::Abort);
    }

    spdlog::info("OpenGL vendor: {}", reinterpret_cast<const char *>(glGetString(GL_VENDOR)));
    spdlog::info("OpenGL renderer: {}", reinterpret_cast<const char *>(glGetString(GL_RENDERER)));
    spdlog::info("OpenGL version: {}", reinterpret_cast<const char *>(glGetString(GL_VERSION)));
    spdlog::info("OpenGL GLSL: {}", reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION)));

    glViewport(0, 0, kScreenWidth, kScreenHeight);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

#if defined(USE_IMGUI)
    IMGUI_CHECKVERSION();

    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGuiIO &io = ImGui::GetIO();

    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init("#version 150");

    bool show_demo_window = true;
    bool show_another_window = false;

    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
#endif

    bool is_running = true;

    SDL_Event event;

    while (is_running) {
        while (SDL_PollEvent(&event)) {
#if defined(USE_IMGUI)
            ImGui_ImplSDL2_ProcessEvent(&event);
#endif

            if (event.type == SDL_QUIT) {
                is_running = false;
            } else if (event.type == SDL_KEYUP) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    is_running = false;
                }
            }
        }

#if defined(USE_IMGUI)
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window) {
            ImGui::ShowDemoWindow(&show_demo_window);
        }

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {
            static float f = 0.0f;
            static int counter = 0;

            ImGui::Begin("Hello, world!");  // Create a window called "Hello, world!" and append into it.

            ImGui::Text("This is some useful text.");                 // Display some text (you can use a format strings too)
            ImGui::Checkbox("Demo Window", &show_demo_window);        // Edit bools storing our window open/close state
            ImGui::Checkbox("Another Window", &show_another_window);

            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);                                // Edit 1 float using a slider from 0.0f to 1.0f
            ImGui::ColorEdit3("clear color", reinterpret_cast<float *>(&clear_color));  // Edit 3 floats representing a color

            if (ImGui::Button("Button")) {  // Buttons return true when clicked (most widgets return true when edited/activated)
                counter++;
            }

            ImGui::SameLine();
            ImGui::Text("counter = %d", counter);

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", static_cast<f64>(1000.0f / io.Framerate), static_cast<f64>(io.Framerate));
            ImGui::End();
        }

        // 3. Show another simple window.
        if (show_another_window)
        {
            ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
            ImGui::Text("Hello from another window!");

            if (ImGui::Button("Close Me")) {
                show_another_window = false;
            }

            ImGui::End();
        }

        ImGui::Render();
#endif

#if defined(USE_IMGUI)
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
#else
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
#endif

        glClear(GL_COLOR_BUFFER_BIT);

#if defined(USE_IMGUI)
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
#endif

        SDL_GL_SwapWindow(window);
    }

#if defined(USE_IMGUI)
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();
#endif

    SDL_GL_MakeCurrent(window, nullptr);
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

