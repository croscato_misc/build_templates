// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#ifndef WATCOM_SRC_DEFS_HPP
#define WATCOM_SRC_DEFS_HPP 1

#include <cstdint>
#include <cstddef>

#define UNUSED(param) ((void)(param))

typedef std::int8_t i8;
typedef std::int16_t i16;
typedef std::int32_t i32;
typedef std::int64_t i64;

typedef std::int8_t s8;
typedef std::int16_t s16;
typedef std::int32_t s32;
typedef std::int64_t s64;

typedef std::uint8_t u8;
typedef std::uint16_t u16;
typedef std::uint32_t u32;
typedef std::uint64_t u64;

typedef float f32;

typedef std::intptr_t iptr;
typedef std::intptr_t sptr;
typedef std::uintptr_t uptr;

typedef std::intptr_t isize;
typedef std::intptr_t ssize;
typedef std::size_t usize;

#endif // WATCOM_SRC_DEFS_HPP

