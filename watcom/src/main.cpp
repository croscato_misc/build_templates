// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#include <iostream>

int
main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    std::cout << "Hello, world!\n";

    return 0;
}

