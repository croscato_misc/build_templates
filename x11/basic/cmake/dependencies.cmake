# Copyright (c) 2023 Gustavo Ribeiro Croscato
# SPDX-License-Identifier: MIT

include(FindGit)

find_package(Git)

if (NOT Git_FOUND)
    message(FATAL_ERROR "Git not found!")
endif ()

include(FetchContent)

set(FETCHCONTENT_QUIET FALSE)

FetchContent_Declare(fmt
    GIT_REPOSITORY https://github.com/fmtlib/fmt.git
    GIT_TAG 9.1.0
    GIT_PROGRESS TRUE
    GIT_SHALLOW TRUE
)

set(FMT_DOC OFF)
set(FMT_INSTALL OFF)
set(FMT_TEST OFF)

FetchContent_MakeAvailable(fmt)

add_compile_definitions(USING_FMT)

set(FETCHCONTENT_FULLY_DISCONNECTED ON CACHE BOOL "Disable dependency update." FORCE)
