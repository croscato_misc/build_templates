// Copyright (c) 2023 Gustavo Ribeiro Croscato
// SPDX-License-Identifier: MIT

#include <string_view>
#include <cstdlib>

#include <fmt/core.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>

template<typename ...Args> [[noreturn]] void
Quit(std::string_view message, Args ...args)
{
    fmt::print(message, args...);
    fmt::print("\n");

   std::exit(127);
}

int
main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    Display *display = XOpenDisplay(nullptr);

    if (display == nullptr) {
        Quit("Can't open X11 display");
    }

    XErrorHandler old_error_handler = XSetErrorHandler([](Display *error_display, XErrorEvent *error_event)->int {
        UNUSED(error_display);

        Quit("X11: request {}.{}, type {}, code {}, serial {}"
            , error_event->request_code
            , error_event->minor_code
            , error_event->type
            , error_event->error_code
            , error_event->serial
        );

        return 127;
    });

    int screen_number = XDefaultScreen(display);

    XVisualInfo visual_template{};

    visual_template.screen = screen_number;

    i32 visual_count = 0;

    XVisualInfo *visual_list = XGetVisualInfo (
          display
        , VisualScreenMask
        , &visual_template
        , &visual_count
    );

    XVisualInfo *visual_item = visual_list;

    int depth  = -1;
    Visual *visual;

    i32 visual_index = -1;

    for (i32 index = 0; index < visual_count; ++index) {
        if (depth < visual_item->depth) {
            depth  = visual_item->depth;
            visual = visual_item->visual;

            visual_index = index;
        }

        ++visual_item;
    }

    if (visual_index >= 0) {
        const char *class_name[] = {
              "StaticGray"
            , "GrayScale"
            , "StaticColor"
            , "PseudoColor"
            , "TrueColor"
            , "DirectColor"
        };

        visual_item = &visual_list[visual_index];

        fmt::print("Visual info: id 0x{:x}, depth {}, class {} ({})\n",
            visual_item->visualid,
            visual_item->depth,
            visual_item->c_class,
            class_name[visual_item->c_class]
        );
    }

    XFree(visual_list);

    Colormap colormap = XCreateColormap(
          display                     // Display
        , XDefaultRootWindow(display) // Window
        , visual                      // Visual type
        , AllocNone                   // Colormap entries (AllocNone, AllocAll)
    );

    constexpr i32 kEventMask =
        KeyPressMask         | KeyReleaseMask         | ButtonPressMask          |
        ButtonReleaseMask    | EnterWindowMask        | LeaveWindowMask          |
        PointerMotionMask    | PointerMotionHintMask  | Button1MotionMask        |
        Button2MotionMask    | Button3MotionMask      | Button4MotionMask        |
        Button5MotionMask    | ButtonMotionMask       | KeymapStateMask          |
        ExposureMask         | VisibilityChangeMask   | StructureNotifyMask      |
        ResizeRedirectMask   | SubstructureNotifyMask | SubstructureRedirectMask |
        FocusChangeMask      | PropertyChangeMask     | ColormapChangeMask       |
        OwnerGrabButtonMask;


    Screen *screen = XDefaultScreenOfDisplay(display);

    XSetWindowAttributes set_window_attributes {
          None                 // background_pixmap
        , screen->white_pixel  // background_pixel
        , CopyFromParent       // border_pixmap
        , screen->black_pixel  // border_pixel
        , ForgetGravity        // bit_gravity
        , NorthWestGravity     // win_gravity
        , NotUseful            // backing_store
        , static_cast<u32>(-1) // backing_planes
        , static_cast<u32>(0)  // backing_pixel
        , False                // save_under
        , kEventMask           // event_mask
        , 0                    // do_not_propagate_mask
        , false                // override_redirect
        , colormap             // colormap
        , None                 // cursor
    };

    constexpr u32 kWindowFlags =
        CWBackPixmap       | CWBackPixel     | /*CWBorderPixmap |*/
        CWBorderPixel      | CWBitGravity    | CWWinGravity   |
        CWBackingStore     | CWBackingPlanes | CWBackingPixel |
        CWOverrideRedirect | CWSaveUnder     | CWEventMask    |
        CWDontPropagate    | CWColormap      | CWCursor;

    i32 screen_width  = XWidthOfScreen(screen);
    i32 screen_height = XHeightOfScreen(screen);
    u32 window_width = 640;
    u32 window_height = 480;

    i32 window_x = (screen_width  / 2) - (static_cast<i32>(window_width)  / 2);
    i32 window_y = (screen_height / 2) - (static_cast<i32>(window_height) / 2);

    Window window = XCreateWindow(
          display                     // Display
        , XDefaultRootWindow(display) // Parent
        , window_x, window_y          // Window X position, Window Y position
        , window_width, window_height // Window width, Window height
        , 0                           // Border width
        , visual_item->depth          // Color depth
        , InputOutput                 // Window class (InputOutput, InputOnly, CopyFromParent)
        , visual_item->visual         // Visual type
        , kWindowFlags                // What attributes are defined
        , &set_window_attributes      // Attributes
    );

    if (window == None) {
        Quit("Can't create a X11 window");
    }

    const char *title = "X11 Sample";

    XSetStandardProperties(
          display // Display
        , window  // Window
        , title   // Window title
        , nullptr // Icon name
        , None    // Icon pixmap
        , nullptr // Application argument list (argv)
        , 0       // Application argument count (argc)
        , nullptr // Size hints
    );

    Atom atom_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", false);
    [[maybe_unused]] Atom atom_wm_name = XInternAtom(display, "_NET_WM_NAME", false);
    [[maybe_unused]] Atom atom_state_fullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", false);

    XSetWMProtocols(display, window, &atom_delete_window, 1);

    XSizeHints size_hints{};

    size_hints.flags = USPosition | USSize;

    XSetWMNormalHints(display, window, &size_hints);

    XWMHints wm_hints{};

    wm_hints.flags = StateHint;
    wm_hints.initial_state = NormalState;

    XSetWMHints(display, window, &wm_hints);

    char application_name[] = "sample";
    char application_class[] = "sample";

    XClassHint hints[] {
        {application_name, application_class}
    };

    XSetClassHint(display, window, hints);

    GC gc = XCreateGC(
          display // display
        , window  // drawable
        , 0       // value mask
        , nullptr // value list
    );

    XSetForeground(display, gc, screen->black_pixel);
    XSetBackground(display, gc, screen->white_pixel);

    XMapWindow(display, window);
    XMoveWindow(display, window, window_x, window_y);

    XEvent event;

    for (;;) {
        XNextEvent(display, &event);

        if (event.type == MapNotify) {
            break;
        }
    }

    bool is_running = true;

    XFontStruct *font = XQueryFont(display, XGContextFromGC(gc));

    const char message[] = "Hello World!";

    while (is_running) {
        while (XEventsQueued(display, QueuedAfterFlush) > 0) {
            XNextEvent(display, &event);

            if (event.type == Expose) {
                XClearWindow(display, window);

                XWindowAttributes window_attributes;

                XGetWindowAttributes(display, window, &window_attributes);

                i32 message_x = static_cast<i32>((window_attributes.width  / 2.0) - (((font->max_bounds.rbearing - font->min_bounds.lbearing) * (static_cast<i32>(sizeof(message)) - 1)) / 2.0));
                i32 message_y = static_cast<i32>((window_attributes.height / 2.0) - ((font->max_bounds.ascent + font->max_bounds.descent) / 2.0));

                XDrawString(
                      display
                    , window
                    , gc
                    , message_x           // x
                    , message_y           // y
                    , message             // string
                    , sizeof(message) - 1 // length
                );
            } else if (event.type == KeyRelease) {
                KeySym keysym = XkbKeycodeToKeysym(
                      display
                    , static_cast<KeyCode>(event.xkey.keycode)
                    , 0
                    , event.xkey.state & ShiftMask ? 1 : 0
                );

                if (keysym == XK_Escape) {
                    is_running = false;
                }
            } else if (event.type == ClientMessage) {
                Atom atom = static_cast<Atom>(event.xclient.data.l[0]);

                if (atom == atom_delete_window) {
                    is_running = false;
                }
            }
        }
    }

    XUnmapWindow(display, window);
    XDestroyWindow(display, window);
    XFreeGC(display, gc);
    XFreeFontInfo(nullptr, font, 1);
    XFreeColormap(display, colormap);
    XSetErrorHandler(old_error_handler);
    XCloseDisplay(display);

    return 0;
}

